
// Nav

$(function($){
    var topnav = $('.nav');
    var label = $('.nav');
    $h = label.offset().top;

    $(window).scroll(function(){
        // Если прокрутили скролл ниже макушки блока, включаем фиксацию

        if ( $(window).scrollTop() > $h) {
            topnav.addClass('fix_top');
        }else{
            //Иначе возвращаем всё назад. Тут вы вносите свои данные
            topnav.removeClass('fix_top');
        }
    });
});




(function() {

    $('.nav_toggle').on('click touchstart', function(e){
        e.preventDefault();
        $('.nav').toggleClass('nav_open');
        $('.header__toggle').toggleClass('nav_open');
    });

}());


$(".btn_modal").fancybox({
    'padding'    : 0
});


var promo = new Swiper ('.promo__slider', {
    loop: true,
    pagination: {
        el: '.swiper-pagination',
        clickable: true
    },
    navigation: {
        nextEl: '.promo_button_next',
        prevEl: '.promo_button_prev',
    },
});


var popular = new Swiper ('.popular_slider', {
    loop: true,
    spaceBetween: 30,
    pagination: {
        el: '.swiper-pagination',
        clickable: true
    },
    navigation: {
        nextEl: '.slider_button_next',
        prevEl: '.slider_button_prev',
    },
    breakpoints: {
        992: {
            spaceBetween: 10,
        },
        767: {
            loop: false,
        }
    }
});


var actions = new Swiper ('.actions__slider', {
    loop: true,
    spaceBetween: 30,
    pagination: {
        el: '.swiper-pagination',
        clickable: true
    },
    navigation: {
        nextEl: '.slider_button_next',
        prevEl: '.slider_button_prev',
    },
});

var salon = new Swiper ('.salon', {
    loop: true,
    spaceBetween: 30,
    slidesPerView: 2,
    slidesPerGroup: 2,
    pagination: {
        el: '.swiper-pagination',
        clickable: true
    },
    navigation: {
        nextEl: '.slider_button_next',
        prevEl: '.slider_button_prev',
    },
    breakpoints: {
        992: {
            slidesPerView: 2,
            slidesPerGroup: 2,
            spaceBetween: 10,
        },
        767: {
            loop: false,
            slidesPerView: 1,
            slidesPerGroup: 1,
            spaceBetween: 10,
        }
    }
});



var news = new Swiper ('.news_block', {
    loop: true,
    slidesPerView: 3,
    spaceBetween: 30,
    slidesPerGroup: 3,
    pagination: {
        el: '.swiper-pagination',
        clickable: true
    },
    // Navigation arrows
    navigation: {
        nextEl: '.slider_button_next',
        prevEl: '.slider_button_prev',
    },
    breakpoints: {
        1170: {
            slidesPerView: 3,
            slidesPerGroup: 3
        },
        992: {
            slidesPerView: 2,
            slidesPerGroup: 2,
            spaceBetween: 10,
        },
        767: {
            loop: false,
            slidesPerView: 1,
            slidesPerGroup: 1
        }
    }
});
