<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="fonts/Circe/stylesheet.css">
        <link rel="stylesheet" href="fonts/GothamPro/GothamPro.css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i,800&display=swap&subset=cyrillic" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Oswald:400,700&display=swap&subset=cyrillic" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/css/swiper.min.css">
        <link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">
        <link rel="stylesheet" href="css/main.css">

    </head>
    <body>

        <div class="page">

            <header class="header">
                <div class="container">
                    <div class="header__row">
                        <a href="#" class="header__toggle nav_toggle">
                            <span></span>
                        </a>
                        <a href="/" class="header__logo">
                            <img src="img/logo.png" class="img_fluid" alt="">
                        </a>
                        <div class="header__text">ОФИЦИАЛЬНЫЙ АВТОДИЛЕР</div>
                        <ul class="header__contact">
                            <li>
                                <a href="tel:+78422270027" class="header__contact_phone">+7 (8422) 27-00-27</a>
                            </li>
                            <li>
                                <a class="header__contact_callback btn_modal" href="#callback">Заказать звонок</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </header>

            <nav class="nav">
                <div class="container">
                    <div class="nav__layout nav_toggle"></div>
                    <div class="nav__content">
                        <ul>
                            <li><a href="#">Главная</a></li>
                            <li><a href="#">Автомобили</a></li>
                            <li><a href="#">Сервис</a></li>
                            <li><a href="#">Акции</a></li>
                            <li><a href="#">Кредит</a></li>
                            <li><a href="#">Страхование</a></li>
                            <li><a href="#">О компании</a></li>
                        </ul>
                    </div>
                </div>
            </nav>

            <section class="promo">
                <div class="promo__slider swiper-container">

                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        <div class="swiper-slide">
                            <div class="promo__slide" style="background-image: url('images/top_slide_01.jpg')">
                                <div class="promo__wrap">
                                    <div class="container">
                                        <div class="promo__content">
                                            <h1 class="promo__heading">НОВАЯ LADA VESTA</h1>
                                            <div class="promo__text">В кредит от 27 000 рублей в месяц</div>
                                            <a href="#" class="btn_red btn_round">Подробнее</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="promo__slide" style="background-image: url('images/top_slide_01.jpg')">
                                <div class="promo__wrap">
                                    <div class="container">
                                        <div class="promo__content">
                                            <h1 class="promo__heading">НОВАЯ LADA VESTA</h1>
                                            <div class="promo__text">В кредит от 27 000 рублей в месяц</div>
                                            <a href="#" class="btn_red btn_round">Подробнее</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="promo__slide" style="background-image: url('images/top_slide_01.jpg')">
                                <div class="promo__wrap">
                                    <div class="container">
                                        <div class="promo__content">
                                            <h1 class="promo__heading">НОВАЯ LADA VESTA</h1>
                                            <div class="promo__text">В кредит от 27 000 рублей в месяц</div>
                                            <a href="#" class="btn_red btn_round">Подробнее</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="promo_button_prev"></div>
                    <div class="promo_button_next"></div>

                    <div class="swiper-pagination"></div>
                </div>
            </section>

            <section class="advantage">
                <div class="container">
                    <ul>
                        <li>
                            <div class="advantage__val"><span>6</span></div>
                            <div class="advantage__text">Автосалонов</div>
                        </li>
                        <li>
                            <div class="advantage__val"><span>6</span></div>
                            <div class="advantage__text">Центров СТО</div>
                        </li>
                        <li>
                            <div class="advantage__val"><span>7</span><span>2</span><span>8</span></div>
                            <div class="advantage__text">Сотрудников</div>
                        </li>
                        <li>
                            <div class="advantage__val"><span>9</span><span>2</span><span>3</span></div>
                            <div class="advantage__text">Авто в наличии</div>
                        </li>
                    </ul>
                </div>
            </section>

            <section class="offer">
                <div class="container">
                    <h2>Быстрый подбор нового автомобиля</h2>
                    <form class="form">
                        <div class="row">
                            <div class="col col-xs-12 col-sm-6 col-md-6 col-lg-3 col-gutter-lr">
                                <div class="form_group">
                                    <select class="form_control form_select">
                                        <option value="1">Новые и с пробегом</option>
                                        <option value="2">Новые и с пробегом</option>
                                        <option value="">Новые и с пробегом</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-sm-6 col-md-6 col-lg-3 col-gutter-lr">
                                <div class="form_group">
                                    <select class="form_control form_select">
                                        <option value="Марка">Марка</option>
                                        <option value="Марка">Марка</option>
                                        <option value="Марка">Марка</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-sm-6 col-md-4 col-lg-3 col-gutter-lr">
                                <div class="form_group">
                                    <select class="form_control form_select">
                                        <option value="1">Модель</option>
                                        <option value="2">Модель</option>
                                        <option value="3">Модель</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-sm-6 col-md-4 col-lg-3 col-gutter-lr">
                                <div class="form_group">
                                    <select class="form_control form_select">
                                        <option value="1">КПП</option>
                                        <option value="2">КПП</option>
                                        <option value="3">КПП</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-sm-6 col-md-4 col-lg-3 col-gutter-lr">
                                <div class="form_group">
                                    <select class="form_control form_select">
                                        <option value="1">Тип кузова</option>
                                        <option value="2">Тип кузова</option>
                                        <option value="3">Тип кузова</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-sm-6 col-md-4 col-lg-3 col-gutter-lr">
                                <div class="form_group">
                                    <select class="form_control form_select">
                                        <option value="1">Цена от</option>
                                        <option value="2">Цена от</option>
                                        <option value="3">Цена от</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-sm-6 col-md-4 col-lg-3 col-gutter-lr">
                                <div class="form_group">
                                    <select class="form_control form_select">
                                        <option value="1">Цена до</option>
                                        <option value="2">Цена до</option>
                                        <option value="3">Цена до</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-sm-6 col-md-4 col-lg-3 col-gutter-lr">
                                <div class="form_group">
                                    <button type="submit" class="btn_red">Показать (30 авто)</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>

            <section class="block block_gray">
                <div class="container">
                    <h2>Наши акции <span class="color_red">(43)</span></h2>
                </div>
                <div class="actions">
                    <div class="actions__slider swiper-container">
                        <!-- Additional required wrapper -->
                        <div class="swiper-wrapper">
                            <!-- Slides -->
                            <div class="swiper-slide">
                                <div class="action_row">

                                    <div class="action action_long action_black" style="background-image: url('images/action__01.jpg')">
                                        <div class="action__content">
                                            <div class="action__title">Абсолютно новый AUDI Q8</div>
                                            <div class="action__lead">Свободен от предрассудков</div>
                                            <div class="action__text">Audi Q6 по спец. цене</div>
                                            <div class="action__button">
                                                <a href="#" class="btn btn_black">Подробнее</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="action action_small" style="background-image: url('images/action__02.jpg')">
                                        <div class="action__content">
                                            <div class="action__title">Premium серии</div>
                                            <div class="action__text">
                                                Audi A3 Sedan Premium — 1 645 000 рублей<br/>
                                                Audi A4 Premium — 2 050 000 рублей<br/>
                                                Audi A5 Sportback Premium — 2 400 000 рублей<br/>
                                                Audi Q5 Premium — 3 200 000 рублей
                                            </div>
                                            <div class="action__button">
                                                <a href="#" class="btn">Подробнее</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="action action_small" style="background-image: url('images/action__03.jpg')">
                                        <div class="action__content">
                                            <div class="action__title">Приглашение на презентацию</div>
                                            <div class="action__lead">Приглашение на презентацию</div>
                                            <div class="action__button">
                                                <a href="#" class="btn">Подробнее</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="action action_long" style="background-image: url('images/action__04.jpg')">
                                        <div class="action__content">
                                            <div class="action__title">Приглашение на презентацию новой Elantra</div>
                                            <div class="action__text">Приглашение на презентацию НОВОЙ ELANTRA!</div>
                                            <div class="action__button">
                                                <a href="#" class="btn">Подробнее</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="action_row">

                                    <div class="action action_long action_black" style="background-image: url('images/action__01.jpg')">
                                        <div class="action__content">
                                            <div class="action__title">Абсолютно новый AUDI Q8</div>
                                            <div class="action__lead">Свободен от предрассудков</div>
                                            <div class="action__text">Audi Q6 по спец. цене</div>
                                            <div class="action__button">
                                                <a href="#" class="btn btn_black">Подробнее</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="action action_small" style="background-image: url('images/action__02.jpg')">
                                        <div class="action__content">
                                            <div class="action__title">Premium серии</div>
                                            <div class="action__text">
                                                Audi A3 Sedan Premium — 1 645 000 рублей<br/>
                                                Audi A4 Premium — 2 050 000 рублей<br/>
                                                Audi A5 Sportback Premium — 2 400 000 рублей<br/>
                                                Audi Q5 Premium — 3 200 000 рублей
                                            </div>
                                            <div class="action__button">
                                                <a href="#" class="btn">Подробнее</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="action action_small" style="background-image: url('images/action__03.jpg')">
                                        <div class="action__content">
                                            <div class="action__title">Приглашение на презентацию</div>
                                            <div class="action__lead">Приглашение на презентацию</div>
                                            <div class="action__button">
                                                <a href="#" class="btn">Подробнее</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="action action_long" style="background-image: url('images/action__04.jpg')">
                                        <div class="action__content">
                                            <div class="action__title">Приглашение на презентацию новой Elantra</div>
                                            <div class="action__text">Приглашение на презентацию НОВОЙ ELANTRA!</div>
                                            <div class="action__button">
                                                <a href="#" class="btn">Подробнее</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="action_row">

                                    <div class="action action_long action_black" style="background-image: url('images/action__01.jpg')">
                                        <div class="action__content">
                                            <div class="action__title">Абсолютно новый AUDI Q8</div>
                                            <div class="action__lead">Свободен от предрассудков</div>
                                            <div class="action__text">Audi Q6 по спец. цене</div>
                                            <div class="action__button">
                                                <a href="#" class="btn btn_black">Подробнее</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="action action_small" style="background-image: url('images/action__02.jpg')">
                                        <div class="action__content">
                                            <div class="action__title">Premium серии</div>
                                            <div class="action__text">
                                                Audi A3 Sedan Premium — 1 645 000 рублей<br/>
                                                Audi A4 Premium — 2 050 000 рублей<br/>
                                                Audi A5 Sportback Premium — 2 400 000 рублей<br/>
                                                Audi Q5 Premium — 3 200 000 рублей
                                            </div>
                                            <div class="action__button">
                                                <a href="#" class="btn">Подробнее</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="action action_small" style="background-image: url('images/action__03.jpg')">
                                        <div class="action__content">
                                            <div class="action__title">Приглашение на презентацию</div>
                                            <div class="action__lead">Приглашение на презентацию</div>
                                            <div class="action__button">
                                                <a href="#" class="btn">Подробнее</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="action action_long" style="background-image: url('images/action__04.jpg')">
                                        <div class="action__content">
                                            <div class="action__title">Приглашение на презентацию новой Elantra</div>
                                            <div class="action__text">Приглашение на презентацию НОВОЙ ELANTRA!</div>
                                            <div class="action__button">
                                                <a href="#" class="btn">Подробнее</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- If we need pagination -->
                        <div class="swiper-pagination"></div>

                        <!-- If we need navigation buttons -->
                        <div class="slider_button_prev"></div>
                        <div class="slider_button_next"></div>

                    </div>

                </div>
            </section>

            <div class="block">
               <div class="container">
                   <h2>Популярные подборки</h2>
                   <div class="popular_inner">
                       <div class="popular_slider swiper-container">
                           <!-- Additional required wrapper -->
                           <div class="swiper-wrapper">
                               <!-- Slides -->
                               <div class="swiper-slide">
                                   <div class="popular">

                                       <a href="#" class="popular__item popular_one hide-xs-only" style="background-image: url('images/popular__01.jpg')">
                                           <div class="popular__content">
                                               <div class="popular__content_title">Семейные</div>
                                               <div class="popular__content_text">125 Авто</div>
                                           </div>
                                       </a>

                                       <a href="#" class="popular__item popular_two" style="background-image: url('images/popular__02.jpg')">
                                           <div class="popular__content">
                                               <div class="popular__content_title">Кроссоверы</div>
                                               <div class="popular__content_text">125 Авто</div>
                                           </div>
                                       </a>

                                       <a href="#" class="popular__item popular_three" style="background-image: url('images/popular__03.jpg')">
                                           <div class="popular__content">
                                               <div class="popular__content_title">Седаны</div>
                                               <div class="popular__content_text">125 Авто</div>
                                           </div>
                                       </a>

                                       <a href="#" class="popular__item popular_four hide-xs-only" style="background-image: url('images/popular__04.jpg')">
                                           <div class="popular__content">
                                               <div class="popular__content_title">Седаны</div>
                                               <div class="popular__content_text">125 Авто</div>
                                           </div>
                                       </a>
                                   </div>
                               </div>
                               <div class="swiper-slide">
                                   <div class="popular">

                                       <a href="#" class="popular__item popular_one hide-xs-only" style="background-image: url('images/popular__01.jpg')">
                                           <div class="popular__content">
                                               <div class="popular__content_title">Семейные</div>
                                               <div class="popular__content_text">125 Авто</div>
                                           </div>
                                       </a>

                                       <a href="#" class="popular__item popular_two" style="background-image: url('images/popular__02.jpg')">
                                           <div class="popular__content">
                                               <div class="popular__content_title">Кроссоверы</div>
                                               <div class="popular__content_text">125 Авто</div>
                                           </div>
                                       </a>

                                       <a href="#" class="popular__item popular_three" style="background-image: url('images/popular__03.jpg')">
                                           <div class="popular__content">
                                               <div class="popular__content_title">Седаны</div>
                                               <div class="popular__content_text">125 Авто</div>
                                           </div>
                                       </a>

                                       <a href="#" class="popular__item popular_four hide-xs-only" style="background-image: url('images/popular__04.jpg')">
                                           <div class="popular__content">
                                               <div class="popular__content_title">Седаны</div>
                                               <div class="popular__content_text">125 Авто</div>
                                           </div>
                                       </a>
                                   </div>
                               </div>
                               <div class="swiper-slide">
                                   <div class="popular">

                                       <a href="#" class="popular__item popular_one hide-xs-only" style="background-image: url('images/popular__01.jpg')">
                                           <div class="popular__content">
                                               <div class="popular__content_title">Семейные</div>
                                               <div class="popular__content_text">125 Авто</div>
                                           </div>
                                       </a>

                                       <a href="#" class="popular__item popular_two" style="background-image: url('images/popular__02.jpg')">
                                           <div class="popular__content">
                                               <div class="popular__content_title">Кроссоверы</div>
                                               <div class="popular__content_text">125 Авто</div>
                                           </div>
                                       </a>

                                       <a href="#" class="popular__item popular_three" style="background-image: url('images/popular__03.jpg')">
                                           <div class="popular__content">
                                               <div class="popular__content_title">Седаны</div>
                                               <div class="popular__content_text">125 Авто</div>
                                           </div>
                                       </a>

                                       <a href="#" class="popular__item popular_four hide-xs-only" style="background-image: url('images/popular__04.jpg')">
                                           <div class="popular__content">
                                               <div class="popular__content_title">Седаны</div>
                                               <div class="popular__content_text">125 Авто</div>
                                           </div>
                                       </a>
                                   </div>
                               </div>
                           </div>
                           <!-- If we need pagination -->
                           <div class="swiper-pagination"></div>

                           <!-- If we need navigation buttons -->
                           <div class="slider_button_prev"></div>
                           <div class="slider_button_next"></div>
                       </div>
                   </div>
               </div>
            </div>

            <section class="block block_gray">
                <div class="container">
                    <h2>6 автосалонов в Ульяновске</h2>
                    <div class="salon_inner">
                        <div class="salon swiper-container">
                            <!-- Additional required wrapper -->
                            <div class="swiper-wrapper">
                                <!-- Slides -->
                                <div class="swiper-slide">
                                    <a href="#" class="salon__item">
                                        <img src="images/auto_01.jpg" class="img_fluid" alt="">
                                        <div class="salon__content">
                                            <div class="salon__title">Hyundai</div>
                                            <div class="salon__text">125 Авто в наличии</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="#" class="salon__item">
                                        <img src="images/auto_02.jpg" class="img_fluid" alt="">
                                        <div class="salon__content">
                                            <div class="salon__title">Kia</div>
                                            <div class="salon__text">125 Авто в наличии</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="#" class="salon__item">
                                        <img src="images/auto_01.jpg" class="img_fluid" alt="">
                                        <div class="salon__content">
                                            <div class="salon__title">Hyundai</div>
                                            <div class="salon__text">125 Авто в наличии</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="#" class="salon__item">
                                        <img src="images/auto_02.jpg" class="img_fluid" alt="">
                                        <div class="salon__content">
                                            <div class="salon__title">Kia</div>
                                            <div class="salon__text">125 Авто в наличии</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="#" class="salon__item">
                                        <img src="images/auto_01.jpg" class="img_fluid" alt="">
                                        <div class="salon__content">
                                            <div class="salon__title">Hyundai</div>
                                            <div class="salon__text">125 Авто в наличии</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="#" class="salon__item">
                                        <img src="images/auto_02.jpg" class="img_fluid" alt="">
                                        <div class="salon__content">
                                            <div class="salon__title">Kia</div>
                                            <div class="salon__text">125 Авто в наличии</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!-- If we need pagination -->
                            <div class="swiper-pagination"></div>

                            <!-- If we need navigation buttons -->
                            <div class="slider_button_prev"></div>
                            <div class="slider_button_next"></div>
                        </div>
                    </div>
                </div>
            </section>
            

            <div class="block">
                <div class="container">
                    <h2>Наши новости</h2>
                    <div class="news_block__inner">
                        <div class="news_block swiper-container">
                            <!-- Additional required wrapper -->
                            <div class="swiper-wrapper">
                                <!-- Slides -->
                                <div class="swiper-slide">
                                    <a href="#" class="news_item">
                                        <div class="news_item__image">
                                            <img src="images/news__image_01.jpg" class="img_fluid" alt="">
                                            <div class="news_item__date">30 июня 2019</div>
                                        </div>
                                        <div class="news_item__title"><span>Марекетолог Kia лучший в Поволжье</span></div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="#" class="news_item">
                                        <div class="news_item__image">
                                            <img src="images/news__image_02.jpg" class="img_fluid" alt="">
                                            <div class="news_item__date">30 июня 2019</div>
                                        </div>
                                        <div class="news_item__title"><span>Была представлена новая Kia Rio</span></div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="#" class="news_item">
                                        <div class="news_item__image">
                                            <img src="images/news__image_03.jpg" class="img_fluid" alt="">
                                            <div class="news_item__date">30 июня 2019</div>
                                        </div>
                                        <div class="news_item__title"><span>Марекетолог Kia лучший в Поволжье</span></div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="#" class="news_item">
                                        <div class="news_item__image">
                                            <img src="images/news__image_01.jpg" class="img_fluid" alt="">
                                            <div class="news_item__date">30 июня 2019</div>
                                        </div>
                                        <div class="news_item__title"><span>Марекетолог Kia лучший в Поволжье</span></div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="#" class="news_item">
                                        <div class="news_item__image">
                                            <img src="images/news__image_02.jpg" class="img_fluid" alt="">
                                            <div class="news_item__date">30 июня 2019</div>
                                        </div>
                                        <div class="news_item__title"><span>Была представлена новая Kia Rio</span></div>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="#" class="news_item">
                                        <div class="news_item__image">
                                            <img src="images/news__image_03.jpg" class="img_fluid" alt="">
                                            <div class="news_item__date">30 июня 2019</div>
                                        </div>
                                        <div class="news_item__title"><span>Марекетолог Kia лучший в Поволжье</span></div>
                                    </a>
                                </div>
                            </div>
                            <!-- If we need pagination -->
                            <div class="swiper-pagination"></div>

                            <!-- If we need navigation buttons -->
                            <div class="slider_button_prev"></div>
                            <div class="slider_button_next"></div>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="footer">
                <div class="footer__top">
                    <div class="container">
                        <div class="footer__row">
                            <div class="footer__info">
                                <a class="footer__logo" href="/">
                                    <img src="img/footer__logo.png" class="img_fluid" alt="">
                                </a>
                                <div class="footer__copy">2019 © АВТОРАЙ | Все права защищены</div>
                            </div>
                            <nav class="footer__nav">
                                <div class="footer__nav_col">
                                    <div class="footer__nav_title"><a href="#">Автомобили</a></div>
                                    <ul>
                                        <li><a href="#">Новые авто</a></li>
                                        <li><a href="#">Авто с пробегом</a></li>
                                        <li><a href="#">Кроссоверы</a></li>
                                        <li><a href="#">Городские</a></li>
                                    </ul>
                                </div>
                                <div class="footer__nav_col">
                                    <div class="footer__nav_title"><a href="#">Услуги</a></div>
                                    <ul>
                                        <li><a href="#">СТО</a></li>
                                        <li><a href="#">Страхование</a></li>
                                        <li><a href="#">Автокредит</a></li>
                                        <li><a href="#">Техосмотр</a></li>
                                    </ul>
                                </div>
                                <div class="footer__nav_col">
                                    <div class="footer__nav_title"><a href="#">О компании</a></div>
                                    <ul>
                                        <li><a href="#">Автосалоны</a></li>
                                        <li><a href="#">О компании</a></li>
                                        <li><a href="#">Вакансии</a></li>
                                        <li><a href="#">Контакты</a></li>
                                    </ul>
                                </div>
                            </nav>
                            <ul class="footer__contact">
                                <li>
                                    <a href="tel:+27-00-27" class="footer__contact_phone">27-00-27</a>
                                </li>
                                <li>
                                    <a class="footer__contact_callback btn_modal" href="#callback">Заказать звонок</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="footer__bottom">
                    <div class="container">
                        <div class="footer__bottom_text">
                            Обращаем ваше внимание на то, что данный Интернет-сайт носит исключительно информационный характер и ни при каких условиях не является публичной офертой, определяемой положениями Статьи 437 Гражданского кодекса Российской Федерации. Для получения подробной информации о стоимости автомобилей Alfa Romeo, Audi, BMW, BMW Motorrad, Chrysler, FORD, Genesis, Hyundai, Jaguar, Jeep, KIA, Land Rover, Lexus, Mazda, Mercedes-Benz, Mitsubishi, Nissan, Porsche, Renault, ŠKODA, Smart, Toyota, Volkswagen обращайтесь к менеджерам по продажам автоцентров «РОЛЬФ». Для получения информации о приобретении автомобилей в кредит, страховании, техническом обслуживании и ремонте автомобилей, запасных частях, дополнительном оборудовании, аксессуарах также обращайтесь в сервисные центры и автосалоны «РОЛЬФ».Права на сайт принадлежат ООО «РОЛЬФ» (ИНН 5047059383, ОГРН 1045009553577 от 29 марта 2004 года) тел. +7 (495) 785-19-78 , адрес эл. почты reception@rolf.ru
                        </div>
                    </div>
                </div>
            </footer>

        </div>


        <!-- Modal Callback -->
        <div class="hide">
            <div class="modal" id="callback">
                <div class="modal__title">Обратный звонок</div>
                <form class="form">
                    <div class="form_group">
                        <input type="text" class="form_control" name="name" placeholder="Имя">
                    </div>
                    <div class="form_group">
                        <input type="text" class="form_control" name="phone" placeholder="Телефон">
                    </div>
                    <button type="submit" class="btn_red">Заказать звонок</button>
                </form>
            </div>
        </div>
        <!-- -->

        <script src="js/vendor/modernizr-3.5.0.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
        <script src="js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/js/swiper.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
